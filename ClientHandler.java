import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.StringTokenizer;


public class ClientHandler extends Thread {
    final DataInputStream server_input; 
    final DataOutputStream client_output; 
    final Socket s; 

    public ClientHandler(Socket s, DataInputStream dis, DataOutputStream dos)  
    { 
        this.s = s; 
        this.server_input = dis; 
        this.client_output = dos; 
    } 

    @Override
    public void run() {
        while(true){
            try {

                
                 // Tunggu input dari master
                String input = server_input.readUTF();
                
                if(input.equalsIgnoreCase("stop")) {
                    System.out.println("Client memasukkan perintah berhenti.");
                    System.out.println("------------------------------------");
                    System.out.println();
                    return;
                }

                System.out.println("------------------------------------");
                System.out.println("Operasi diterima : " + input);

                // gunakan StringTokenizer untuk memisahlan request menjadi angka dan operatornya
                StringTokenizer st = new StringTokenizer(input);
                int angka1 = Integer.parseInt(st.nextToken());
                String operator = st.nextToken(); 
                int angka2 = Integer.parseInt(st.nextToken());

                int sleep = Integer.parseInt(st.nextToken());
                sleep(sleep);
                
                int result = 0;
                // Tipe-tipe jobnya.

                System.out.println("Operasi dikomputasi...");
                switch(operator) {
                    case "+":
                        result = angka1 + angka2;
                        break;
                    case "-":
                        result = angka1 - angka2;
                        break;
                    case "*":
                        result = angka1 * angka2;
                        break;
                    case "/":
                        result = angka1 / angka2;
                        break;
                }
                System.out.println("Kirimkan hasil...");

                // kembalikan result ke master atau client.
                client_output.writeUTF(Integer.toString(result));

            } catch (SocketException e){
                try{
                    s.close();
                } catch (IOException er){
                    er.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } 
            catch (InterruptedException e){
                e.printStackTrace();
            }

            
           
        }
    
    }   
}



