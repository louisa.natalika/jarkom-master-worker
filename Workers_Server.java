//Workers
//Simple Calculator menggunakan TCP
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

public class Workers_Server {
    public static void main(String args[]) throws IOException {

        // Step 1: Establish socket connection.
        ServerSocket ss = new ServerSocket(Integer.parseInt(args[0]));
       
        while (true) {

            Socket s = null;

            try {
                // s = ss.accept();
                
                int counter=0;
                while(true){
                    s = ss.accept();
                    counter++;

                    System.out.println("------------------------------------");
                    System.out.println();
                    System.out.println("Terkoneksi dengan Client...");

					// Step 2: Memproses operation request.
					DataOutputStream client_output = new DataOutputStream(s.getOutputStream());
					DataInputStream server_input = new DataInputStream(s.getInputStream());
					
					System.out.println("Thread untuk Client No :" +  counter+ " dimulai...");

                    System.out.println("Memasang thread untuk client...");
                    Thread tr = new ClientHandler(s, server_input, client_output);
					
                    tr.start();
                    
				}
               
            }
            catch (Exception e){
                s.close();
            }
        }
    }
}
