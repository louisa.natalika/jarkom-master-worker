//Master node
//Simple Calculator menggunakan TCP
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.Random;

public class Master_Client {
    public static void main(String[] args) throws IOException {
        InetAddress ip = InetAddress.getLocalHost();
        Scanner sc = new Scanner(System.in);

        int[] ports = {6868, 6969};

        try {
            int rnd = new Random().nextInt(ports.length);
            int port = ports[rnd];

            // Step 1: Open socket connection
            Socket s = new Socket(ip, port);

            // Step 2: Communication untuk  input dan output stream ke workers
            DataOutputStream client_output = new DataOutputStream(s.getOutputStream());
            DataInputStream server_input = new DataInputStream(s.getInputStream());
            
            System.out.println("----------------------------------------------------------------------");
            System.out.println("Aplikasi ini merupakan calculator yang dapat menerima dua buah operand");
            System.out.println("----------------------------------------------------------------------");
            System.out.println();

            while (true) {
                // Keterangan masukan input sesuai format
                System.out.println("Ketik 'stop' bila anda ingin berhenti");
                System.out.print("Masukkan operasi yang ingin dilakukan: "); 
                System.out.println("'angka1 operator angka2'"); //format
                System.out.print("input: ");

                String input = sc.nextLine();

                
    			//jika ingin menghentikan program/proses
                if (input.equals("stop")) {
                    client_output.writeUTF(input);
                    s.close();
                    break;
                }

                System.out.print("sleep: ");
                int sleep = Integer.parseInt(sc.nextLine());

                String kirim = input + " " + sleep;

                // kirimkan operasi yang diinginkan ke workers
                client_output.writeUTF(kirim);

                // tunggu sampai request operation diproses dan mengembalikan hasil ke master
                String total = server_input.readUTF();
                System.out.println("Hasil: " + total);
                System.out.println("Diperoleh dari port: " + port);
                System.out.println("----------------------------------------------------------------------");
                System.out.println();
                
            }
            s.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
