# Calculator Application

## Deskripsi

Aplikasi ini merupakan kalkulator sederhana yang menerima perintah operasi dari Client. Operasi yang diterima hanya operasi yang melibatkan dua buah operand. Operator yang dapat digunakan adalah '+', '-', '*', '/'.

## Cara Menggunakan

1. Clone repositori: 

`git clone https://gitlab.com/louisa.natalika/jarkom-master-worker.git`

2. Compile setiap program:
```
javac ClientHandler.java
javac Master_Client.java
javac Workers_Server.java
```



3. Buka minimal 3 buah terminal

4. Jalankan program di setiap terminal yang berbeda \n
```
java Workers_Server 6868 
java Workers_Server 6969
java Master_Client
```


## Contoh Eksekusi Program
- Master_Client
```
> java Master_Client
> ----------------------------------------------------------------------
> Aplikasi ini merupakan calculator yang dapat menerima dua buah operand
> ----------------------------------------------------------------------
> 
> Ketik 'stop' bila anda ingin berhenti
> Masukkan operasi yang ingin dilakukan: 'angka1 operator angka2'
> input: 1 + 5
> sleep: 1000
> Hasil: 6
> Diperoleh dari port: 6969
> ----------------------------------------------------------------------
> 
> Ketik 'stop' bila anda ingin berhenti
> Masukkan operasi yang ingin dilakukan: 'angka1 operator angka2'
> input: 2 / 2
> sleep: 1000
> Hasil: 1
> Diperoleh dari port: 6969
> ----------------------------------------------------------------------
> 
> Ketik 'stop' bila anda ingin berhenti
> Masukkan operasi yang ingin dilakukan: 'angka1 operator angka2'
> input: stop
```

- Workers_Server 6969

```
> java Workers_Server 6969
> ------------------------------------
> 
> Terkoneksi dengan Client...
> Thread untuk Client No :1 dimulai...
> Memasang thread untuk client...
> ------------------------------------
> Operasi diterima : 1 + 5 1000
> Operasi dikomputasi...
> Kirimkan hasil...
> ------------------------------------
> Operasi diterima : 2 / 2 1000
> Operasi dikomputasi...
> Kirimkan hasil...
> Client memasukkan perintah berhenti.
> ------------------------------------
```


## Dibuat Oleh
- **Ashila Ghassani** - 1806205395
- **Kezia Sulami** - 1806133755
- **Louisa Natalika J** - 1806205022